<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>PHP Form Handling</title>
  </head>
  <body>
    <div class="container">
      <!-- form method attribute defines the type of HTTP request to be made -->
      <!-- form action attribute defines WHERE the request will be sent to -->
      <form method="POST" action="./index_action.php">
       <!-- <div class="form-group">
          <label for="fName">First name:</label>
          <input type="text" class="form-control" id="fName" name="first_name">
        </div>  

        <div class="form-group">
          <label for="lName">First name:</label>
          <input type="text" class="form-control" id="lName" name="last_name">
        </div>  

        <button type="submit" class="btn btn-primary">Submit</button> -->
        <h1>Activity 1</h1>
        <hr>
        <div class="form-group">
           <label for="month">Month:</label>
           <input type="text" class="form-control" id="month" name="month">
         </div>  

         <div class="form-group">
           <label for="day">Day:</label>
           <input type="text" class="form-control" id="day" name="day">
         </div>  

         <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>